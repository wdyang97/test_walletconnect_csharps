﻿using Nethereum.Contracts;
using Nethereum.Util;
using Nethereum.Web3;
using Nethereum.Web3.Accounts;
using QRCoder;
using System;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using WalletConnectSharp.Core.Models;
using WalletConnectSharp.Desktop;
using WalletConnectSharp.Examples.Examples;
using WalletConnectSharp.NEthereum;

namespace WalletConnectSharp.Examples
{
    class Program


    {
        private static readonly IExample[] Examples = new IExample[]
        {
            new NEthereumSendTransactionExample()
        };

        static void ShowHelp()
        {
            Console.WriteLine("Please specify which example to run");
            foreach (var e in Examples)
            {
                Console.WriteLine("    - " + e.Name);
            }
        }

        static async Task Main(string[] args)
        {   
            Console.Write("Test wallet");
            var clientMeta = new ClientMeta()
            {
                Name = "Lumiworld Application",
                Description = "An example that showcases how to use the WalletConnectSharp library",
                Icons = new[] { "https://i.ibb.co/jrT49rC/l-Pn-PH0-Z8-Rsu-Ks-EQRwa-PS-e-R0-Qe5-IDHf6pao-CA.jpg" },
                URL = "https://lumiworld.io/"
            };

            var client = new WalletConnect(clientMeta);
            Console.WriteLine(client);
            Console.WriteLine(client.URI);
            //QRCodeGenerator qrGenerator = new QRCodeGenerator();
            //QRCodeData data = qrGenerator.CreateQrCode(client.URI, QRCodeGenerator.ECCLevel.Q);
            //QRCode code = new QRCode(data);
            //Bitmap bmp = code.GetGraphic(5);
            //bmp.Save(@"C:\Users\phduo\OneDrive\Desktop\image.bmp");
            await client.Connect();
            Console.WriteLine("Connect sussces");
            var rpcEndpoint = "https://data-seed-prebsc-2-s1.binance.org:8545/";
            Console.WriteLine("The account " + client.Accounts[0] + " has connected!");
            Console.WriteLine("Using RPC endpoint " + rpcEndpoint + " as the fallback RPC endpoint");
            var fromAddress = client.Accounts[0];
            //We use an External Account so we can sign transactions

            var web3 = client.BuildWeb3(new Uri(rpcEndpoint)).AsWalletAccount(true);
            //var web3 = new Web3(client.CreateProvider(new Uri(rpcEndpoint)));
            //var bal = web3.Eth.GetBalance("0x6cb014f6910615ae3fb5d9a1a4f0a2e1ad97c8eb");
            //var privateKey = "58639e61f14466815d91efa8f118554cf71f605c4795a68f3729abcafce5dad5";
            //var account = new Account(privateKey, chainId: 97);
            //var fromAddress = account.Address;
            //var web3 = new Web3(account, rpcEndpoint);

            var abi = "[{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"_owner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"_spender\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_value\",\"type\":\"uint256\"}],\"name\":\"Approval\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"_from\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"_to\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_value\",\"type\":\"uint256\"}],\"name\":\"Transfer\",\"type\":\"event\"},{\"constant\":false,\"inputs\":[{\"internalType\":\"address\",\"name\":\"_spender\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_value\",\"type\":\"uint256\"}],\"name\":\"approve\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"internalType\":\"address\",\"name\":\"_spender\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_value\",\"type\":\"uint256\"}],\"name\":\"decreaseAllowance\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"internalType\":\"address\",\"name\":\"_spender\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_value\",\"type\":\"uint256\"}],\"name\":\"increaseAllowance\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"internalType\":\"address\",\"name\":\"_to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_value\",\"type\":\"uint256\"}],\"name\":\"transfer\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"_success\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"internalType\":\"address\",\"name\":\"_from\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_value\",\"type\":\"uint256\"}],\"name\":\"transferFrom\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"_success\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_mainchainGateway\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"constant\":true,\"inputs\":[{\"internalType\":\"address\",\"name\":\"_owner\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_spender\",\"type\":\"address\"}],\"name\":\"allowance\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"_value\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"balanceOf\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"decimals\",\"outputs\":[{\"internalType\":\"uint8\",\"name\":\"\",\"type\":\"uint8\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"mainchainGateway\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"name\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"symbol\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"totalSupply\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"}]";
            var ContractAddress = "0x3E69F099BB1114273BD4e91125d6dAe4d37a01dC";
            var contract = web3.Eth.GetContract(abi, ContractAddress);
            var balanceFunction = contract.GetFunction("balanceOf");
            var balanceFirstAmountSend = await balanceFunction.CallAsync<object>(fromAddress);
            Console.WriteLine($"Account {fromAddress} balance: {balanceFirstAmountSend}");
            var amountToSend = Nethereum.Web3.Web3.Convert.ToWei(1000, UnitConversion.EthUnit.Ether);

            Console.WriteLine("Transfering 1000 tokens" + amountToSend);
            var transferFunction = contract.GetFunction("transfer");
            var newAddress = "0x3E33EBD125F5c85cDA07aF3Bf87bABfC3977511a";
            //var gas = await function.EstimateGasAsync(from, null, null);
            var transactionManager = web3.TransactionManager;
            var gas = await transferFunction.EstimateGasAsync(fromAddress, null, null, newAddress, amountToSend);
            Console.WriteLine("Gas " + gas);
            web3.TransactionManager.UseLegacyAsDefault = true; //Ok, got it. You must set web3.TransactionManager.UseLegacyAsDefault = true and skip settings manually gas fee. Enjoy :-)

            var receiptFirstAmountSend = await transferFunction.SendTransactionAndWaitForReceiptAsync(fromAddress, gas, null, null, newAddress, amountToSend);
            var txHash = receiptFirstAmountSend.TransactionHash;
            Console.WriteLine("Hash:" + txHash);
            var newBalance = await balanceFunction.CallAsync<object>(newAddress);
            Console.WriteLine($"Account {newAddress} balance: {newBalance}");
            if (args.Length == 0)
            {
                ShowHelp();
                return;
            }

            string name = args[0];
            string[] exampleArgs = args.Skip(1).ToArray();

            var example = Examples.FirstOrDefault(e => e.Name.ToLower() == name);

            if (example == null)
            {
                ShowHelp();
                return;
            }
            
            await example.Execute(exampleArgs);
        }
    }
}